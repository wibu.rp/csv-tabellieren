public class TabelliereCSV {
	
	private static String[] einfuegeUnterstrich(String[] TabelleOhneStrich, String Unterstrich) {
		String[] FertigeTabelle = new String[TabelleOhneStrich.length + 1];
		
		FertigeTabelle[0] = TabelleOhneStrich[0];
		FertigeTabelle[1] = Unterstrich;
		for (int i = 2; i < FertigeTabelle.length; i++) {
			FertigeTabelle[i] = TabelleOhneStrich[i - 1];
		}
		return FertigeTabelle;
	}
	
	public static String[] tabelliereCSV(String[] CSVZeilen) {
		String[][] StringMatrix = CSV.parseCSV(CSVZeilen);
		Matrix matrix = new Matrix(StringMatrix);
		String[] TabelleOhneStrich = matrix.generiereTabellenZeilen();
		String Unterstrich = matrix.generiereUnterstrich();
		String[] FertigeTabelle = einfuegeUnterstrich(TabelleOhneStrich, Unterstrich);
		return FertigeTabelle;
	}
}
