public class CSV {
	
	public static String[][] parseCSV(String[] csvZeilen) {
		String[][] Matrix = new String[csvZeilen.length][];
		for (int i = 0; i < csvZeilen.length; i++) {
			Matrix[i] = csvZeilen[i].split(";");
		}
		return Matrix;
	}
}