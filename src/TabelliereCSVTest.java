import static org.junit.Assert.*;
import org.junit.Test;

public class TabelliereCSVTest {

	@Test
	public void testTabelliereCSV() {
		String[] testCSV = new String[]{
				"Name;Strasse",
				"Peter Pan;Hang 5",
		};
		String[] ErwarteteTabelle = new String[]{
				"Name     |Strasse|",
				"---------+-------+",
				"Peter Pan|Hang 5 |",
		};
		String[] SchoeneTabelle = TabelliereCSV.tabelliereCSV(testCSV);
		assertArrayEquals(ErwarteteTabelle, SchoeneTabelle);
	}
}
