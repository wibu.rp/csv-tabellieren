
public class Matrix {
	private String[][] Matrix;
	private int[] SpaltenBreiten;

	public Matrix(String[][] Matrix) {
		this.Matrix = Matrix;
		this.SpaltenBreiten = ermittleSpaltenBreiten();
	}

	private int[] ermittleSpaltenBreiten() {
		int[] SpaltenBreiten = new int[Matrix[0].length];
		
		for (int row = 0; row < Matrix.length; row++) {
			for (int column = 0; column < Matrix[row].length; column++) {
				if (Matrix[row][column].length() > SpaltenBreiten[column]) { 
					SpaltenBreiten[column] = Matrix[row][column].length();
				}
			}
		}
		return SpaltenBreiten;
	}
	
	public String[] generiereTabellenZeilen() {
		String[] Tabelle = new String[Matrix.length];
		
		for (int row = 0; row < Matrix.length; row++) {
			Tabelle[row] = "";
			for (int column = 0; column < Matrix[row].length; column++) {
				Tabelle[row] += String.format("%-"+SpaltenBreiten[column]+"s|", Matrix[row][column]);
			}
		}
		return Tabelle;
	}

	public String generiereUnterstrich() {
		String Unterstrich = "";
		
		for (int column = 0; column < Matrix[0].length; column++) {
			for (int i = 0; i < SpaltenBreiten[column]; i++) Unterstrich += "-";
			Unterstrich += "+";
		}
		return Unterstrich;
	}
}
